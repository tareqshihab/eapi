<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            "name" => $this->name,
            "price" => $this->price,
            "stock" => $this->stock > 0 ? $this->stock:'Out of Stock',
            "discount" => $this->discount,
            "totalPrice" => round($this->price-(($this->price*$this->discount)/100),2),
            "ratings" => $this->reviews->count() >0 ? round($this->reviews->sum('star')/$this->reviews->count(),2):'No rating yet',
            "href" =>[
                "reviews" => route('products.show',$this->id)
            ]
        ];
    }

}
