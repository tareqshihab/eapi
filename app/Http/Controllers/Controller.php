<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function successResponse($result, $message, $status_code)
    {
        $response = [
            'stausCode'=>$status_code,
            'status'=>'success',
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, $status_code);
    }
}
